package pl.test.shoppinglist.domain.usecase

import io.reactivex.Completable
import pl.test.shoppinglist.data.repository.ShoppingListDataRepository
import pl.test.shoppinglist.domain.model.ShoppingList
import javax.inject.Inject

class UpdateShoppingList @Inject constructor(private val shoppingListDataRepository: ShoppingListDataRepository)
    : UseCase<Completable, ShoppingList> {
    override fun buildUseCaseObservable(params: ShoppingList?): Completable {
        return shoppingListDataRepository.updateShoppingList(params!!)

    }
}