package pl.test.shoppinglist.domain.usecase

import io.reactivex.Single
import pl.test.shoppinglist.data.repository.ShoppingListDataRepository
import pl.test.shoppinglist.domain.model.ShoppingList
import javax.inject.Inject

class ShoppingArchivedListsUseCase @Inject constructor(private val shoppingListDataRepository: ShoppingListDataRepository)
    : UseCase<Single<MutableList<ShoppingList>>, Void> {
    override fun buildUseCaseObservable(params: Void?): Single<MutableList<ShoppingList>> {
        return shoppingListDataRepository.getAll(true)
    }
}