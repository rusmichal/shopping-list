package pl.test.shoppinglist.domain.usecase

interface UseCase<T, in Params> {
    fun buildUseCaseObservable(params: Params? = null): T
}