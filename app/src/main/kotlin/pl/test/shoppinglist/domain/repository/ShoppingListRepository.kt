package pl.test.shoppinglist.domain.repository

import io.reactivex.Completable
import io.reactivex.Single
import pl.test.shoppinglist.domain.model.ShoppingList

interface ShoppingListRepository {
    fun clearShoppingLists(): Completable
    fun deleteShoppingList(shoppingList: ShoppingList): Completable
    fun createShoppingList(shoppingList: ShoppingList): Completable
    fun updateShoppingList(shoppingList: ShoppingList): Completable
    fun getAll(isArchived : Boolean = false): Single<MutableList<ShoppingList>>
    fun getShoppingList(id: String): Single<ShoppingList>
}