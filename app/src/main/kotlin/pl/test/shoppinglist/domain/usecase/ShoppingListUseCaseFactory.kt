package pl.test.shoppinglist.domain.usecase

import io.reactivex.Completable
import io.reactivex.Single
import pl.test.shoppinglist.data.repository.ShoppingListDataRepository
import pl.test.shoppinglist.domain.model.ShoppingList
import javax.inject.Inject

class ShoppingListUseCaseFactory @Inject constructor(private val shoppingListDataRepository: ShoppingListDataRepository) {
    fun createShoppingActiveListsUseCase(): UseCase<Single<MutableList<ShoppingList>>, Void> {
        return ShoppingActiveListsUseCase(shoppingListDataRepository)
    }

    fun createShoppingArchivedListsUseCase(): UseCase<Single<MutableList<ShoppingList>>, Void> {
        return ShoppingArchivedListsUseCase(shoppingListDataRepository)
    }

    fun createShoppingListByIdUseCase(): UseCase<Single<ShoppingList>, String> {
        return ShoppingListById(shoppingListDataRepository)
    }

    fun createUpdateShoppingListUseCase(): UseCase<Completable, ShoppingList> {
        return UpdateShoppingList(shoppingListDataRepository)
    }
}