package pl.test.shoppinglist.domain.usecase

import io.reactivex.Single
import pl.test.shoppinglist.data.repository.ShoppingListDataRepository
import pl.test.shoppinglist.domain.model.ShoppingList
import javax.inject.Inject

class ShoppingListById @Inject constructor(private val shoppingListDataRepository: ShoppingListDataRepository)
    : UseCase<Single<ShoppingList>, String> {
    override fun buildUseCaseObservable(params: String?): Single<ShoppingList> {
        return shoppingListDataRepository.getShoppingList(params!!)

    }
}