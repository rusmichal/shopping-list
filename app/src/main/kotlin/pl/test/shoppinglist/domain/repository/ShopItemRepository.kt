package pl.test.shoppinglist.domain.repository

import io.reactivex.Completable
import io.reactivex.Single
import pl.test.shoppinglist.domain.model.ShopItem

interface ShopItemRepository {
    fun clearShopItems() : Completable
    fun deleteShopItem(id : Int) : Completable
    fun createShopItem(shopItem : ShopItem) : Completable
    fun updateShopItem(shopItem : ShopItem) : Completable
    fun saveCollection(shopItem : MutableCollection<ShopItem>) : Completable
    fun getAll() : Single<MutableCollection<ShopItem>>
    fun getShopItem(id: Int) : Single<ShopItem>
}