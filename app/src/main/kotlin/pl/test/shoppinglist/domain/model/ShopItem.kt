package pl.test.shoppinglist.domain.model

data class ShopItem(var id: String?, var name: String? = "") {
    constructor(name: String?) : this(null, name)
}