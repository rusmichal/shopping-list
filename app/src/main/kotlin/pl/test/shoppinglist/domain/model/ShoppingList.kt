package pl.test.shoppinglist.domain.model

data class ShoppingList(var id: String?, var name: String, var isArchived: Boolean, var shopItems: MutableList<ShopItem>) {

}