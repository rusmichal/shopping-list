package pl.test.shoppinglist.presentation.ui.shopping.details

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_shopping_list_details.*
import pl.test.shoppinglist.domain.model.ShopItem
import pl.test.shoppinglist.presentation.ui.adapters.ShopItemAdapter
import pl.test.shoppinglist.presentation.ui.shopping.details.presenters.ShoppingListDetailsPresenter
import pl.test.shoppinglist.presentation.ui.shopping.details.views.ShoppingListDetailsView
import pl.test.shoppinglist.shoppinglist.R
import javax.inject.Inject

class FragmentShoppingListDetails : Fragment(), ShoppingListDetailsView {
    override fun notifyShoppingListChanged(shopItems: MutableList<ShopItem>) {
        val adapter = rvShopItemList.adapter as ShopItemAdapter
        adapter.clear()
        adapter.addAll(shopItems)
        adapter.notifyDataSetChanged()

        rvShopItemList.visibility = View.VISIBLE
    }

    @Inject lateinit var shoppingListDetailsPresenter: ShoppingListDetailsPresenter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun hideEmptyView() {
        tvEmptyView.visibility = View.GONE
    }

    override fun hideError() {
        tvErrorView.visibility = View.GONE
    }

    override fun showEmptyView() {
        rvShopItemList.visibility = View.GONE
        tvEmptyView.visibility = View.VISIBLE
        tvErrorView.visibility = View.GONE
    }

    override fun showError() {
        rvShopItemList.visibility = View.GONE
        tvErrorView.visibility = View.VISIBLE
        tvEmptyView.visibility = View.GONE
    }

    override fun displayShopItems(shopItems: MutableList<ShopItem>) {
        val adapter = rvShopItemList.adapter as ShopItemAdapter
        adapter.addAll(shopItems)
        adapter.notifyDataSetChanged()

        rvShopItemList.visibility = View.VISIBLE
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_shopping_list_details, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view?.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
        view?.isClickable = true

        initializeRecyclerView()
        initializeViews()

        lifecycle.addObserver(shoppingListDetailsPresenter)
    }

    private fun initializeViews() {
        btnAdd.setOnClickListener {
            val shopItemName = edShopItemName.text.trim().toString()
            if (shoppingListDetailsPresenter.validateShopItem(shopItemName)) {
                shoppingListDetailsPresenter.createShopItem(shopItemName)
            }
        }
    }

    private fun initializeRecyclerView() {
        val layoutManager = LinearLayoutManager(context)

        val adapter = ShopItemAdapter()
        adapter.onRemoveItemClickListener = {
            shoppingListDetailsPresenter.removeShopItem(it)
        }
        layoutManager.isAutoMeasureEnabled = false

        rvShopItemList.isNestedScrollingEnabled = false
        rvShopItemList.layoutManager = layoutManager
        rvShopItemList.adapter = adapter
        rvShopItemList.setHasFixedSize(true)
        rvShopItemList.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
    }

    override fun onDestroyView() {
        lifecycle.removeObserver(shoppingListDetailsPresenter)
        super.onDestroyView()
    }

    companion object {
        val SHOPPING_ID_KEY = "shoppingListId"

        fun newInstance(shoppingListId: String?): FragmentShoppingListDetails = FragmentShoppingListDetails().apply {
            arguments = Bundle().apply {
                putString(SHOPPING_ID_KEY, shoppingListId)
            }
        }
    }
}