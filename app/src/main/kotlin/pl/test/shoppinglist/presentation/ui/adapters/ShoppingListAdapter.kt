package pl.test.shoppinglist.presentation.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_shopping_list.view.*
import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.shoppinglist.R

class ShoppingListAdapter : RecyclerView.Adapter<ShoppingListAdapter.ShoppingListViewHolder>() {
    var onItemClickListener: ((model: ShoppingList) -> Unit)? = null
    private var shoppingLists: MutableList<ShoppingList> = mutableListOf()

    override fun onBindViewHolder(holder: ShoppingListViewHolder?, position: Int) {
        holder?.bindItem(shoppingLists[position], onItemClickListener, position)
    }

    override fun getItemCount(): Int {
        return shoppingLists.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ShoppingListViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.item_shopping_list, parent, false)
        return ShoppingListAdapter.ShoppingListViewHolder(itemView)

    }

    class ShoppingListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(model: ShoppingList, onItemClickListener: ((model: ShoppingList) -> Unit)?, position: Int) {
            with(itemView) {
                tvShoppingListmName.text = model.name
                setOnClickListener({ onItemClickListener?.invoke(model) })
            }
        }
    }

    fun addAll(shoppingLists: MutableList<ShoppingList>): Boolean {
        return this.shoppingLists.addAll(shoppingLists)
    }
}