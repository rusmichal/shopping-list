package pl.test.shoppinglist.presentation.ui.shopping.activity


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dagger.android.*
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import pl.test.shoppinglist.presentation.ui.adapters.ShoppingFragmentAdapter
import pl.test.shoppinglist.presentation.ui.shopping.active.FragmentShoppingActiveList
import pl.test.shoppinglist.presentation.ui.shopping.archived.FragmentShoppingArchivedList
import pl.test.shoppinglist.presentation.ui.shopping.presenters.ShoppingPresenter
import pl.test.shoppinglist.presentation.ui.shopping.views.ShoppingView
import pl.test.shoppinglist.shoppinglist.R
import javax.inject.Inject


class ShoppingActivity : AppCompatActivity(), ShoppingView, HasSupportFragmentInjector {
    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector

    }

    @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>
    @Inject lateinit var shoppingPresenter: ShoppingPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lifecycle.addObserver(shoppingPresenter)

        initializeViewPager()
        initializeTabLayout()
    }

    private fun initializeViewPager() {
        val shoppingFragmentAdapter = ShoppingFragmentAdapter(supportFragmentManager, mutableListOf("Current", "Archived"))
        shoppingFragmentAdapter.add(FragmentShoppingActiveList.newInstance())
        shoppingFragmentAdapter.add(FragmentShoppingArchivedList.newInstance())

        viewPager.adapter = shoppingFragmentAdapter
    }

    private fun initializeTabLayout() {
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(shoppingPresenter)
    }
}
