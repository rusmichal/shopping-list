package pl.test.shoppinglist.presentation.injection.modules

import dagger.Module
import dagger.Provides
import pl.test.shoppinglist.data.mapper.ShopItemMapper
import pl.test.shoppinglist.data.repository.ShopItemDataRepository
import pl.test.shoppinglist.data.source.ShopItemDataSourceFactory
import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.injection.scopes.PerActivity
import pl.test.shoppinglist.presentation.ui.shopping.activity.ShoppingActivity
import pl.test.shoppinglist.presentation.ui.shopping.presenters.ShoppingPresenter
import pl.test.shoppinglist.presentation.ui.shopping.views.ShoppingView


@Module
open class ShoppingActivityModule {

    @PerActivity
    @Provides
    internal fun providerShoppingView(shoppingActivity: ShoppingActivity): ShoppingView {
        return shoppingActivity
    }

    @PerActivity
    @Provides
    internal fun providerShoppingPresenter(shoppingView: ShoppingView, useCaseFactory: ShoppingListUseCaseFactory): ShoppingPresenter {
        return ShoppingPresenter(shoppingView, useCaseFactory)
    }

    @PerActivity
    @Provides
    fun provideShopItemDataRepository(shopItemDataSourceFactory: ShopItemDataSourceFactory
                                      , shopItemMapper: ShopItemMapper): ShopItemDataRepository {
        return ShopItemDataRepository(shopItemDataSourceFactory, shopItemMapper)
    }
}