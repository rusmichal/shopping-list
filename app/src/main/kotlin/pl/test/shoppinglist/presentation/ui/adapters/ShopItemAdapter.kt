package pl.test.shoppinglist.presentation.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_shop_list.view.*
import pl.test.shoppinglist.domain.model.ShopItem
import pl.test.shoppinglist.shoppinglist.R

class ShopItemAdapter : RecyclerView.Adapter<ShopItemAdapter.ShopItemViewHolder>() {
    var shopItems: MutableList<ShopItem> = mutableListOf()
    var onRemoveItemClickListener: ((model: ShopItem) -> Unit)? = null

    override fun onBindViewHolder(holder: ShopItemViewHolder?, position: Int) {
        holder?.bindItem(shopItems[position], onRemoveItemClickListener, position)
    }

    override fun getItemCount(): Int {
        return shopItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ShopItemViewHolder {
        val itemView = LayoutInflater.from(parent?.context).inflate(R.layout.item_shop_list, parent, false)
        return ShopItemViewHolder(itemView)
    }

    fun addAll(shopItems: MutableList<ShopItem>): Boolean {
        return this.shopItems.addAll(shopItems)
    }

    fun clear() {
        return this.shopItems.clear()
    }

    class ShopItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(model: ShopItem, onRemoveItemClickListener: ((model: ShopItem) -> Unit)?, position: Int) {
            with(itemView) {
                tvShopItemName.text = model.name
                btnRemove.setOnClickListener({
                    onRemoveItemClickListener?.invoke(model)
                })
            }
        }
    }
}