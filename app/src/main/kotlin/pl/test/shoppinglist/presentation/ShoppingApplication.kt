package pl.test.shoppinglist.presentation

import android.app.Activity
import android.app.Application
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.realm.Realm
import io.realm.RealmConfiguration
import pl.test.shoppinglist.data.entity.InitialDataTransaction
import pl.test.shoppinglist.presentation.injection.DaggerApplicationComponent
import javax.inject.Inject

class ShoppingApplication : Application(), HasActivityInjector {
    @Inject lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        Logger.addLogAdapter(AndroidLogAdapter())

        DaggerApplicationComponent.builder()
                .application(this)
                .build()
                .inject(this)

        initializeRealm()
    }

    private fun initializeRealm() {
        Realm.init(this)
        Realm.setDefaultConfiguration(
                RealmConfiguration.Builder()
                        .name("ShoppingList.db")
                        .initialData(InitialDataTransaction())
                        .deleteRealmIfMigrationNeeded()
                        .build())
    }
}