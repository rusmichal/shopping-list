package pl.test.shoppinglist.presentation.injection.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.test.shoppinglist.presentation.injection.scopes.PerActivity
import pl.test.shoppinglist.presentation.ui.shopping.activity.ShoppingActivity

@Module
abstract class ActivityBindingModule {
    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(ShoppingActivityModule::class, ShoppingActiveListFragmentModule::class,
            ShoppingActiveListFragmentModule::class, ShoppingArchivedListFragmentModule::class, ShoppingDetailsListFragmentModule::class))
    abstract fun bindShoppingActivity(): ShoppingActivity

}