package pl.test.shoppinglist.presentation.ui.shopping.active

import android.content.Context
import android.os.Bundle
import android.view.View
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_recyclerview.*
import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.presentation.ui.adapters.ShoppingListAdapter
import pl.test.shoppinglist.presentation.ui.fragments.FragmentShoppingList
import pl.test.shoppinglist.presentation.ui.shopping.active.presenters.ShoppingActiveListPresenter
import pl.test.shoppinglist.presentation.ui.shopping.active.views.ShoppingActiveListView
import pl.test.shoppinglist.presentation.ui.shopping.activity.ShoppingActivity
import pl.test.shoppinglist.presentation.ui.shopping.details.FragmentShoppingListDetails
import pl.test.shoppinglist.presentation.ui.showFragment
import javax.inject.Inject

class FragmentShoppingActiveList : FragmentShoppingList(), ShoppingActiveListView {
    @Inject lateinit var shoppingActiveListPresenter: ShoppingActiveListPresenter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun createAdapter(): ShoppingListAdapter {
        val adapter = ShoppingListAdapter()
        adapter.onItemClickListener = {
            (context as ShoppingActivity).showFragment(FragmentShoppingListDetails.newInstance(it.id))
        }

        return adapter
    }

    override fun displayShoppingList(shoppingLists: MutableList<ShoppingList>) {
        adapter?.addAll(shoppingLists)
        adapter?.notifyDataSetChanged()

        recyclerView.visibility = View.VISIBLE
    }

    override fun showEmptyView() {
        recyclerView.visibility = View.GONE
        tvEmptyView.visibility = View.VISIBLE
        tvErrorView.visibility = View.GONE
    }

    override fun hideEmptyView() {
        tvEmptyView.visibility = View.GONE
    }

    override fun showError() {
        recyclerView.visibility = View.GONE
        tvErrorView.visibility = View.VISIBLE
        tvEmptyView.visibility = View.GONE
    }

    override fun hideError() {
        tvErrorView.visibility = View.GONE

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(shoppingActiveListPresenter)
    }

    override fun onDestroyView() {
        lifecycle.removeObserver(shoppingActiveListPresenter)
        super.onDestroyView()
    }

    companion object {
        fun newInstance(): FragmentShoppingActiveList {
            return FragmentShoppingActiveList()
        }

    }
}