package pl.test.shoppinglist.presentation.injection.modules

import dagger.Module
import dagger.Provides
import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.injection.scopes.PerActivity
import pl.test.shoppinglist.presentation.ui.shopping.active.FragmentShoppingActiveList
import pl.test.shoppinglist.presentation.ui.shopping.active.presenters.ShoppingActiveListPresenter
import pl.test.shoppinglist.presentation.ui.shopping.active.views.ShoppingActiveListView

@Module
open class ShoppingActiveListFragmentModule {

    @PerActivity
    @Provides
    internal fun providerShoppingActiveList(fragmentShoppingActiveList: FragmentShoppingActiveList): ShoppingActiveListView {
        return fragmentShoppingActiveList
    }

    @PerActivity
    @Provides
    internal fun providerShoppingArchivedListPresenter(shoppingActiveListView: ShoppingActiveListView, useCaseFactory: ShoppingListUseCaseFactory): ShoppingActiveListPresenter {
        return ShoppingActiveListPresenter(shoppingActiveListView, useCaseFactory)
    }
}