package pl.test.shoppinglist.presentation.injection.components

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pl.test.shoppinglist.presentation.ui.shopping.details.FragmentShoppingListDetails

@Subcomponent
interface ShoppingDetailsListFragmentSubComponent : AndroidInjector<FragmentShoppingListDetails> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<FragmentShoppingListDetails>()
}