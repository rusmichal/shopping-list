package pl.test.shoppinglist.presentation.injection.modules

import dagger.Module
import dagger.Provides
import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.injection.scopes.PerActivity
import pl.test.shoppinglist.presentation.ui.shopping.archived.FragmentShoppingArchivedList
import pl.test.shoppinglist.presentation.ui.shopping.archived.presenters.ShoppingArchivedListPresenter
import pl.test.shoppinglist.presentation.ui.shopping.archived.views.ShoppingArchivedListView

@Module
open class ShoppingArchivedListFragmentModule {

    @PerActivity
    @Provides
    internal fun providerShoppingArchivedList(fragmentShoppingArchivedList: FragmentShoppingArchivedList): ShoppingArchivedListView {
        return fragmentShoppingArchivedList
    }

    @PerActivity
    @Provides
    internal fun providerShoppingActiveListPresenter(shoppingArchivedListView: ShoppingArchivedListView, useCaseFactory: ShoppingListUseCaseFactory): ShoppingArchivedListPresenter {
        return ShoppingArchivedListPresenter(shoppingArchivedListView, useCaseFactory)
    }
}