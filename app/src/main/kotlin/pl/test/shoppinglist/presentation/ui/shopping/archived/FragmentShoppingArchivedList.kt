package pl.test.shoppinglist.presentation.ui.shopping.archived

import android.content.Context
import android.os.Bundle
import android.view.View
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_recyclerview.*
import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.presentation.ui.adapters.ShoppingListAdapter
import pl.test.shoppinglist.presentation.ui.fragments.FragmentShoppingList
import pl.test.shoppinglist.presentation.ui.shopping.archived.presenters.ShoppingArchivedListPresenter
import pl.test.shoppinglist.presentation.ui.shopping.archived.views.ShoppingArchivedListView
import javax.inject.Inject

class FragmentShoppingArchivedList : FragmentShoppingList(), ShoppingArchivedListView {

    @Inject lateinit var shoppingArchivedListPresenter: ShoppingArchivedListPresenter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun createAdapter(): ShoppingListAdapter {
        return ShoppingListAdapter()
    }

    override fun displayShoppingList(shoppingLists: MutableList<ShoppingList>) {
        adapter?.addAll(shoppingLists)
        adapter?.notifyDataSetChanged()

        recyclerView.visibility = View.VISIBLE
    }

    override fun hideEmptyView() {
        tvEmptyView.visibility = View.GONE
    }

    override fun hideError() {
        tvErrorView.visibility = View.GONE
    }

    override fun showError() {
        recyclerView.visibility = View.GONE
        tvErrorView.visibility = View.VISIBLE
        tvEmptyView.visibility = View.GONE
    }

    override fun showEmptyView() {
        recyclerView.visibility = View.GONE
        tvEmptyView.visibility = View.VISIBLE
        tvErrorView.visibility = View.GONE
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycle.addObserver(shoppingArchivedListPresenter)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(shoppingArchivedListPresenter)
    }

    companion object {
        fun newInstance(): FragmentShoppingArchivedList {
            return FragmentShoppingArchivedList()
        }
    }
}