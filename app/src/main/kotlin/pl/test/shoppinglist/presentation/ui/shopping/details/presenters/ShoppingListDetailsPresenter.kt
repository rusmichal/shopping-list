package pl.test.shoppinglist.presentation.ui.shopping.details.presenters

import android.os.Bundle
import android.text.TextUtils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import pl.test.shoppinglist.domain.model.ShopItem
import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.ui.BasePresenter
import pl.test.shoppinglist.presentation.ui.shopping.details.FragmentShoppingListDetails
import pl.test.shoppinglist.presentation.ui.shopping.details.views.ShoppingListDetailsView
import javax.inject.Inject

class ShoppingListDetailsPresenter @Inject constructor(val shoppingListDetailsView: ShoppingListDetailsView,
                                                       private val useCaseFactory: ShoppingListUseCaseFactory,
                                                       private val bundle: Bundle) : BasePresenter() {

    private val shoppingActiveListsUseCase = useCaseFactory.createShoppingListByIdUseCase()
    private val updateShoppingListUseCase = useCaseFactory.createUpdateShoppingListUseCase()
    private val compositeDisposable = CompositeDisposable()
    private var currentShoppingList: ShoppingList? = null

    override fun onCreate() {
        super.onCreate()
        val shoppingListId = bundle.getString(FragmentShoppingListDetails.SHOPPING_ID_KEY, "")

        compositeDisposable.add(shoppingActiveListsUseCase.buildUseCaseObservable(shoppingListId)
                .subscribeWith(object : DisposableSingleObserver<ShoppingList>() {
                    override fun onError(error: Throwable) {
                        shoppingListDetailsView.showError()
                    }

                    override fun onSuccess(shoppingList: ShoppingList) {
                        currentShoppingList = shoppingList

                        shoppingListDetailsView.hideError()
                        if (shoppingList.shopItems.isEmpty()) {
                            shoppingListDetailsView.showEmptyView()
                        } else {
                            shoppingListDetailsView.hideEmptyView()
                            shoppingListDetailsView.displayShopItems(shoppingList.shopItems)
                        }
                    }
                }))
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
        super.onDestroy()
    }

    fun addShopItem(item: ShopItem) {
    }

    fun removeShopItem(item: ShopItem) {
        currentShoppingList?.shopItems?.remove(item)
        updateShoppingList()
    }

    fun validateShopItem(name: String?): Boolean {
        return !TextUtils.isEmpty(name)
    }

    fun createShopItem(shopItemName: String) {
        currentShoppingList?.shopItems?.add(ShopItem(shopItemName))
        updateShoppingList()
    }

    private fun updateShoppingList() {
        compositeDisposable.add(updateShoppingListUseCase
                .buildUseCaseObservable(currentShoppingList)
                .subscribeWith(object : DisposableCompletableObserver() {
                    override fun onComplete() {
                        shoppingListDetailsView.hideError()
                        shoppingListDetailsView.hideEmptyView()
                        shoppingListDetailsView.notifyShoppingListChanged(currentShoppingList?.shopItems!!)
                    }

                    override fun onError(e: Throwable) {
                        //TODO: try it again
                    }
                }))
    }
}