package pl.test.shoppinglist.presentation.injection.components

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pl.test.shoppinglist.presentation.ui.shopping.active.FragmentShoppingActiveList

@Subcomponent
interface ShoppingActiveListFragmentSubComponent : AndroidInjector<FragmentShoppingActiveList> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<FragmentShoppingActiveList>()
}