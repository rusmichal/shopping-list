package pl.test.shoppinglist.presentation.ui.shopping.active.presenters

import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.ui.BasePresenter
import pl.test.shoppinglist.presentation.ui.shopping.active.views.ShoppingActiveListView
import javax.inject.Inject

class ShoppingActiveListPresenter @Inject constructor(val shoppingActiveListView: ShoppingActiveListView,
                                                      useCaseFactory: ShoppingListUseCaseFactory) : BasePresenter() {

    private val shoppingActiveListsUseCase = useCaseFactory.createShoppingActiveListsUseCase()
    private var disposable: Disposable? = null

    override fun onCreate() {
        super.onCreate()
        disposable = shoppingActiveListsUseCase.buildUseCaseObservable().subscribeWith(object : DisposableSingleObserver<MutableList<ShoppingList>>() {
            override fun onSuccess(list: MutableList<ShoppingList>) {
                shoppingActiveListView.hideError()
                if (list.size == 0) {
                    shoppingActiveListView.showEmptyView()
                } else {
                    shoppingActiveListView.hideEmptyView()
                    shoppingActiveListView.displayShoppingList(list)
                }
            }

            override fun onError(e: Throwable) {
                shoppingActiveListView.showError()
            }

        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        if (!disposable?.isDisposed!!) {
            disposable?.dispose()
        }
        super.onDestroy()
    }
}