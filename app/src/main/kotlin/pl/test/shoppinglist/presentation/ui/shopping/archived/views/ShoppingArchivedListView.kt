package pl.test.shoppinglist.presentation.ui.shopping.archived.views

import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.presentation.ui.BaseView

interface ShoppingArchivedListView : BaseView {
    fun showError()
    fun showEmptyView()
    fun hideEmptyView()
    fun hideError()
    fun displayShoppingList(shoppingLists : MutableList<ShoppingList>)
}