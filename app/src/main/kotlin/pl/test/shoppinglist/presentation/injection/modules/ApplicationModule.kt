package pl.test.shoppinglist.presentation.injection.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.test.shoppinglist.presentation.injection.scopes.PerApplication

@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }
}