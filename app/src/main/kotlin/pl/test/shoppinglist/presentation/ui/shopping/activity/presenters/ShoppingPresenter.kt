package pl.test.shoppinglist.presentation.ui.shopping.presenters

import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.ui.BasePresenter
import pl.test.shoppinglist.presentation.ui.shopping.views.ShoppingView
import javax.inject.Inject

class ShoppingPresenter @Inject constructor(val shoppingView: ShoppingView, private val useCaseFactory: ShoppingListUseCaseFactory) : BasePresenter() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}