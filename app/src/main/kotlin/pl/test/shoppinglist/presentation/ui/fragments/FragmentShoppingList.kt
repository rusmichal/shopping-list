package pl.test.shoppinglist.presentation.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_recyclerview.*
import pl.test.shoppinglist.presentation.ui.adapters.ShoppingListAdapter
import pl.test.shoppinglist.shoppinglist.R

abstract class FragmentShoppingList: Fragment() {
    var adapter : ShoppingListAdapter ? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_recyclerview, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = createAdapter()
        initializeRecyclerView()
    }

    private fun initializeRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        layoutManager.isAutoMeasureEnabled = false
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
    }

    abstract fun createAdapter() : ShoppingListAdapter
}