package pl.test.shoppinglist.presentation.injection.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.test.shoppinglist.presentation.injection.scopes.PerActivity
import pl.test.shoppinglist.presentation.ui.shopping.active.FragmentShoppingActiveList
import pl.test.shoppinglist.presentation.ui.shopping.archived.FragmentShoppingArchivedList
import pl.test.shoppinglist.presentation.ui.shopping.details.FragmentShoppingListDetails

@Module
abstract class FragmentBindingModule {
    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(ShoppingActiveListFragmentModule::class))
    abstract fun bindFragmentShoppingActiveList(): FragmentShoppingActiveList

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(ShoppingArchivedListFragmentModule::class))
    abstract fun bindFragmentShoppingArchivedList(): FragmentShoppingArchivedList

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(ShoppingDetailsListFragmentModule::class))
    abstract fun bindFragmentShoppingListDetails(): FragmentShoppingListDetails

}