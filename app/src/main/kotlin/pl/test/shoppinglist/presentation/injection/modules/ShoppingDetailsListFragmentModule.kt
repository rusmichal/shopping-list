package pl.test.shoppinglist.presentation.injection.modules

import android.os.Bundle
import dagger.Module
import dagger.Provides
import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.injection.scopes.PerActivity
import pl.test.shoppinglist.presentation.ui.shopping.details.FragmentShoppingListDetails
import pl.test.shoppinglist.presentation.ui.shopping.details.presenters.ShoppingListDetailsPresenter
import pl.test.shoppinglist.presentation.ui.shopping.details.views.ShoppingListDetailsView

@Module
open class ShoppingDetailsListFragmentModule {

    @PerActivity
    @Provides
    internal fun providerShoppingListDetails(fragmentShoppingListDetails: FragmentShoppingListDetails): ShoppingListDetailsView {
        return fragmentShoppingListDetails
    }

    @PerActivity
    @Provides
    internal fun providerShoppingListDetailsExtras(fragmentShoppingListDetails: FragmentShoppingListDetails): Bundle {
        return fragmentShoppingListDetails.arguments
    }

    @PerActivity
    @Provides
    internal fun providerShoppingActiveListPresenter(shoppingListDetailsView: ShoppingListDetailsView, useCaseFactory: ShoppingListUseCaseFactory, bundle: Bundle): ShoppingListDetailsPresenter {
        return ShoppingListDetailsPresenter(shoppingListDetailsView, useCaseFactory, bundle)
    }
}