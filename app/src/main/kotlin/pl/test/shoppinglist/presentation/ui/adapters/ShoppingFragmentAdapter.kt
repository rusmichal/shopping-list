package pl.test.shoppinglist.presentation.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ShoppingFragmentAdapter(fragmentManager: FragmentManager, private val titles : MutableList<String>) : FragmentPagerAdapter(fragmentManager) {
    private val items : MutableList<Fragment> = ArrayList(2)

    override fun getItem(position: Int): Fragment {
        return items[position]
    }

    override fun getCount(): Int {
        return items.size
    }

    fun add(fragment: Fragment) {
        items.add(fragment)
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titles[position % titles.size]
    }
}