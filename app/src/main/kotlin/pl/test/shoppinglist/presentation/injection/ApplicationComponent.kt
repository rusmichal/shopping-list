package pl.test.shoppinglist.presentation.injection

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import pl.test.shoppinglist.presentation.ShoppingApplication
import pl.test.shoppinglist.presentation.injection.modules.ActivityBindingModule
import pl.test.shoppinglist.presentation.injection.modules.ApplicationModule
import pl.test.shoppinglist.presentation.injection.modules.FragmentBindingModule
import pl.test.shoppinglist.presentation.injection.scopes.PerApplication

@PerApplication
@Component(modules = arrayOf(ApplicationModule::class,
        AndroidInjectionModule::class,
        ActivityBindingModule::class, AndroidSupportInjectionModule::class,
        FragmentBindingModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): ApplicationComponent
    }

    fun inject(app: ShoppingApplication)
}