package pl.test.shoppinglist.presentation.injection.components

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pl.test.shoppinglist.presentation.ui.shopping.archived.FragmentShoppingArchivedList

@Subcomponent
interface ShoppingArchivedListFragmentSubComponent : AndroidInjector<FragmentShoppingArchivedList> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<FragmentShoppingArchivedList>()
}