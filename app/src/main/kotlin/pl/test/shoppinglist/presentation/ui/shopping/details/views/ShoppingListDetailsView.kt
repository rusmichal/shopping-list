package pl.test.shoppinglist.presentation.ui.shopping.details.views

import pl.test.shoppinglist.domain.model.ShopItem
import pl.test.shoppinglist.presentation.ui.BaseView

interface ShoppingListDetailsView : BaseView {
    fun showError()
    fun showEmptyView()
    fun hideEmptyView()
    fun hideError()
    fun displayShopItems(shopItems: MutableList<ShopItem>)
    fun notifyShoppingListChanged(shopItems: MutableList<ShopItem>)

}