package pl.test.shoppinglist.presentation.ui

import android.support.v4.app.Fragment
import pl.test.shoppinglist.presentation.ui.shopping.activity.ShoppingActivity
import pl.test.shoppinglist.shoppinglist.R

fun ShoppingActivity.showFragment(fragment: Fragment, fragmentName: String? = null) {
    val fragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.replace(R.id.root, fragment)
    fragmentTransaction.addToBackStack(fragmentName)
    fragmentTransaction.commit()
}
