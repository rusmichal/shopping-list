package pl.test.shoppinglist.presentation.ui.shopping.archived.presenters

import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.domain.usecase.ShoppingListUseCaseFactory
import pl.test.shoppinglist.presentation.ui.BasePresenter
import pl.test.shoppinglist.presentation.ui.shopping.archived.views.ShoppingArchivedListView
import javax.inject.Inject

class ShoppingArchivedListPresenter @Inject constructor(val shoppingArchivedListView: ShoppingArchivedListView,
                                                        useCaseFactory: ShoppingListUseCaseFactory) : BasePresenter() {

    private val shoppingArchivedListsUseCase = useCaseFactory.createShoppingArchivedListsUseCase()
    private var disposable: Disposable? = null

    override fun onCreate() {
        super.onCreate()
        disposable = shoppingArchivedListsUseCase.buildUseCaseObservable().subscribeWith(object : DisposableSingleObserver<MutableList<ShoppingList>>() {
            override fun onSuccess(list: MutableList<ShoppingList>) {
                shoppingArchivedListView.hideError()
                if (list.size == 0) {
                    shoppingArchivedListView.showEmptyView()
                } else {
                    shoppingArchivedListView.hideEmptyView()
                    shoppingArchivedListView.displayShoppingList(list)
                }
            }

            override fun onError(e: Throwable) {
                shoppingArchivedListView.showError()
            }

        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        if(!disposable?.isDisposed!!) {
            disposable?.dispose()
        }
        super.onDestroy()
    }
}