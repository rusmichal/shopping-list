package pl.test.shoppinglist.presentation.injection.components

import dagger.Subcomponent
import dagger.android.AndroidInjector
import pl.test.shoppinglist.presentation.ui.shopping.activity.ShoppingActivity

@Subcomponent
interface ShoppingActivitySubComponent : AndroidInjector<ShoppingActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<ShoppingActivity>()
}