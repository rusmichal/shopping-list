package pl.test.shoppinglist.data.source

import pl.test.shoppinglist.data.mapper.ShoppingListMapper
import javax.inject.Inject

open class ShoppingListDataSourceFactory @Inject constructor(val shoppingListMapper: ShoppingListMapper) {

    fun createRealmDataStore(): ShoppingListDataSource {
        return ShoppingListRealmData(shoppingListMapper)
    }
}