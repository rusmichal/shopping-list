package pl.test.shoppinglist.data.source

import com.orhanobut.logger.Logger
import io.reactivex.Completable
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmResults
import pl.test.shoppinglist.data.entity.ShoppingListEntity
import pl.test.shoppinglist.data.mapper.ShoppingListMapper
import pl.test.shoppinglist.domain.model.ShoppingList

class ShoppingListRealmData(private val shoppingListMapper: ShoppingListMapper) : ShoppingListDataSource {
    private val realm: Realm = Realm.getDefaultInstance()

    override fun clearShoppingLists(): Completable {
        return Completable.create { emiter ->
            val result = realm.where(ShoppingListEntity::class.java).findAll()
            realm.executeTransaction({
                result.deleteAllFromRealm()
                emiter.onComplete()
            })
        }
    }

    override fun deleteShoppingList(shoppingList: ShoppingList): Completable {
        return Completable.create { emiter ->
            val shoppingListEntity = realm.where(ShoppingListEntity::class.java).equalTo("id", shoppingList.id).findFirst()
            realm.executeTransaction({
                shoppingListEntity?.deleteFromRealm()
                emiter.onComplete()
            })
        }
    }

    override fun createShoppingList(shoppingList: ShoppingList): Completable {
        return Completable.create { emiter ->
            realm.executeTransaction { realm ->
                val shoppingListEntity = shoppingListMapper.mapToEntity(shoppingList)
                realm.insert(shoppingListEntity)
                emiter.onComplete()
            }
        }
    }

    override fun getAll(isArchived: Boolean): Single<MutableList<ShoppingList>> {
        return Single.defer<MutableList<ShoppingList>> {
            val shoppingListEntities = realm.where(ShoppingListEntity::class.java)
                    .equalTo("isArchived", isArchived)
                    .findAll()
                    .sort("created")

            Single.just<RealmResults<ShoppingListEntity>>(shoppingListEntities)
                    .map {
                        it.map { shoppingListMapper.mapFromEntity(it) }.toMutableList()
                    }
        }
    }

    override fun getShoppingList(id: String): Single<ShoppingList> {
        return Single.defer<ShoppingList> {
            val shoppingListEntity = realm.where(ShoppingListEntity::class.java)
                    .equalTo("id", id)
                    .findFirst()

            Single.just<ShoppingListEntity>(shoppingListEntity)
                    .map {
                        shoppingListMapper.mapFromEntity(it)
                    }
        }
    }

    override fun updateShoppingList(shoppingList: ShoppingList): Completable {
        return Completable.defer {
            val shoppingListEntity = shoppingListMapper.mapToEntity(shoppingList)
            realm.executeTransaction({
                it.copyToRealmOrUpdate(shoppingListEntity)
            })
            Completable.complete()
        }
    }
}