package pl.test.shoppinglist.data.entity

import com.google.gson.annotations.Expose
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class ShoppingListEntity(id: String, isArchived: Boolean, name: String, shopItems: RealmList<ShopItemEntity>?) : RealmObject() {

    constructor() : this("", false, "", null)

    @Expose
    var created: Date? = null
    @PrimaryKey
    @Expose
    var id: String? = null
    @Expose
    var isArchived: Boolean = false
    @Expose
    var name: String? = null
    @Expose
    var shopItems: RealmList<ShopItemEntity>? = RealmList()

    init {
        this.created = Date()
        this.id = id
        this.isArchived = isArchived
        this.name = name
        this.shopItems = shopItems
    }
}