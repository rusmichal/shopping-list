package pl.test.shoppinglist.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import pl.test.shoppinglist.data.mapper.ShopItemMapper
import pl.test.shoppinglist.data.source.ShopItemDataSource
import pl.test.shoppinglist.data.source.ShopItemDataSourceFactory
import pl.test.shoppinglist.domain.model.ShopItem
import pl.test.shoppinglist.domain.repository.ShopItemRepository
import javax.inject.Inject

class ShopItemDataRepository @Inject constructor(shopItemDataStoreFactory: ShopItemDataSourceFactory,
                                                 private val shopItemMapper: ShopItemMapper) : ShopItemRepository {
    private var shopItemDataSource: ShopItemDataSource = shopItemDataStoreFactory.createRealmDataStore()

    override fun createShopItem(shopItem: ShopItem): Completable {
        return shopItemDataSource.createShopItem(shopItemMapper.mapToEntity(shopItem))
    }

    override fun clearShopItems(): Completable {
        return shopItemDataSource.clearShopItems()
    }

    override fun deleteShopItem(id: Int): Completable {
        return shopItemDataSource.deleteShopItem(id)
    }

    override fun getAll(): Single<MutableCollection<ShopItem>> {
        return shopItemDataSource.getAll().map { it.map { shopItemMapper.mapFromEntity(it) }.toMutableList() }
    }

    override fun getShopItem(id: Int): Single<ShopItem> {
        return shopItemDataSource.getShopItem(id).map { shopItemMapper.mapFromEntity(it) }
    }

    override fun saveCollection(shopItem: MutableCollection<ShopItem>): Completable {
        val shopItemEntities = shopItem.map { shopItemMapper.mapToEntity(it) }.toMutableList()
        return shopItemDataSource.saveCollection(shopItemEntities)
    }

    override fun updateShopItem(shopItem: ShopItem): Completable {
        return shopItemDataSource.updateShopItem(shopItemMapper.mapToEntity(shopItem))
    }
}