package pl.test.shoppinglist.data.entity

import com.orhanobut.logger.Logger
import io.realm.Realm
import java.util.*

class InitialDataTransaction : Realm.Transaction {
    override fun execute(realm: Realm?) {
        val firstShoppingListEntity = realm?.createObject(ShoppingListEntity::class.java, UUID.randomUUID().toString())
        firstShoppingListEntity?.name = "First shopping list"
        val firstShopItem = realm?.createObject(ShopItemEntity::class.java, UUID.randomUUID().toString())
        firstShopItem?.name = "First Item"
        val secondShopItem = realm?.createObject(ShopItemEntity::class.java, UUID.randomUUID().toString())
        secondShopItem?.name = "Second Item"
        firstShoppingListEntity?.shopItems?.addAll(arrayListOf(firstShopItem, secondShopItem))

        val secondShoppingListEntity = realm?.createObject(ShoppingListEntity::class.java, UUID.randomUUID().toString())
        secondShoppingListEntity?.name = "Second shopping list"

        val archivedShoppingListEntity = realm?.createObject(ShoppingListEntity::class.java, UUID.randomUUID().toString())
        archivedShoppingListEntity?.name = "Archived shopping list"
        archivedShoppingListEntity?.isArchived = true

    }
}