package pl.test.shoppinglist.data.mapper

import io.realm.RealmList
import pl.test.shoppinglist.data.entity.ShopItemEntity
import pl.test.shoppinglist.data.entity.ShoppingListEntity
import pl.test.shoppinglist.domain.model.ShoppingList
import java.util.*
import javax.inject.Inject

class ShoppingListMapper @Inject constructor(private val shopItemMapper: ShopItemMapper) : Mapper<ShoppingListEntity, ShoppingList> {
    override fun mapFromEntity(type: ShoppingListEntity): ShoppingList {
        val shopItems = type.shopItems?.map { shopItemMapper.mapFromEntity(it) }!!.toMutableList()
        return ShoppingList(type.id, type.name!!, type.isArchived, shopItems)
    }

    override fun mapToEntity(type: ShoppingList): ShoppingListEntity {

        val realmShopItemEntities = RealmList<ShopItemEntity>()
        type.shopItems.map {
            realmShopItemEntities.add(shopItemMapper.mapToEntity(it))
        }

        return ShoppingListEntity(type.id!!, type.isArchived, type.name, realmShopItemEntities)
    }
}