package pl.test.shoppinglist.data.repository

import io.reactivex.Completable
import io.reactivex.Single
import pl.test.shoppinglist.data.source.ShoppingListDataSource
import pl.test.shoppinglist.data.source.ShoppingListDataSourceFactory
import pl.test.shoppinglist.domain.model.ShoppingList
import pl.test.shoppinglist.domain.repository.ShoppingListRepository
import javax.inject.Inject

class ShoppingListDataRepository @Inject constructor(shoppingListDataSourceFactory: ShoppingListDataSourceFactory) : ShoppingListRepository {
    private var shoppingListDataSource: ShoppingListDataSource = shoppingListDataSourceFactory.createRealmDataStore()

    override fun getAll(isArchived : Boolean): Single<MutableList<ShoppingList>> {
        return shoppingListDataSource.getAll(isArchived)
    }

    override fun clearShoppingLists(): Completable {
        return shoppingListDataSource.clearShoppingLists()
    }

    override fun deleteShoppingList(shoppingList: ShoppingList): Completable {
        return shoppingListDataSource.deleteShoppingList(shoppingList)
    }

    override fun createShoppingList(shoppingList: ShoppingList): Completable {
        return shoppingListDataSource.createShoppingList(shoppingList)
    }

    override fun updateShoppingList(shoppingList: ShoppingList): Completable {
        return shoppingListDataSource.updateShoppingList(shoppingList)
    }

    override fun getShoppingList(id: String): Single<ShoppingList> {
        return shoppingListDataSource.getShoppingList(id)
    }
}