package pl.test.shoppinglist.data.mapper

import pl.test.shoppinglist.data.entity.ShopItemEntity
import pl.test.shoppinglist.domain.model.ShopItem
import java.util.*
import javax.inject.Inject

open class ShopItemMapper @Inject constructor() : Mapper<ShopItemEntity, ShopItem> {
    override fun mapFromEntity(type: ShopItemEntity): ShopItem {
        return ShopItem(type.id, type.name!!)
    }

    override fun mapToEntity(type: ShopItem): ShopItemEntity {
        return ShopItemEntity(UUID.randomUUID().toString(), type.name!!)
    }
}