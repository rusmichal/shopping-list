package pl.test.shoppinglist.data.entity

import com.google.gson.annotations.Expose
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class ShopItemEntity(id: String, name: String) : RealmObject() {

    constructor() : this("", "")

    @PrimaryKey
    @Expose
    var id: String? = null
    @Expose
    var name: String? = null

    init {
        this.id = id
        this.name = name
    }
}