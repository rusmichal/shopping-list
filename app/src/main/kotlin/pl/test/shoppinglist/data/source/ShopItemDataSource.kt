package pl.test.shoppinglist.data.source

import io.reactivex.Completable
import io.reactivex.Single
import pl.test.shoppinglist.data.entity.ShopItemEntity

interface ShopItemDataSource {
    fun clearShopItems(): Completable
    fun createShopItem(shopItemEntity: ShopItemEntity): Completable
    fun deleteShopItem(id: Int): Completable
    fun getAll(): Single<MutableList<ShopItemEntity>>
    fun getShopItem(id: Int): Single<ShopItemEntity>
    fun saveCollection(shopItemEntities: MutableList<ShopItemEntity>): Completable
    fun updateShopItem(shopItemEntity: ShopItemEntity): Completable
}