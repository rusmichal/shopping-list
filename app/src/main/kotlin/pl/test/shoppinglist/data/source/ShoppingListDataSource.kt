package pl.test.shoppinglist.data.source

import io.reactivex.Completable
import io.reactivex.Single
import pl.test.shoppinglist.domain.model.ShoppingList

interface ShoppingListDataSource {
    fun clearShoppingLists() : Completable
    fun deleteShoppingList(shoppingList: ShoppingList) : Completable
    fun createShoppingList(shoppingList : ShoppingList) : Completable
    fun getAll(isArchived : Boolean = false): Single<MutableList<ShoppingList>>
    fun getShoppingList(id: String): Single<ShoppingList>
    fun updateShoppingList(shoppingList : ShoppingList) : Completable
}