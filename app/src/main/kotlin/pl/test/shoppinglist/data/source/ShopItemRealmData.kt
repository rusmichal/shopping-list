package pl.test.shoppinglist.data.source

import io.reactivex.Completable
import io.reactivex.Single
import io.realm.Realm
import pl.test.shoppinglist.data.entity.ShopItemEntity

class ShopItemRealmData() : ShopItemDataSource {
    private val realm: Realm = Realm.getDefaultInstance()

    override fun clearShopItems(): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createShopItem(shopItemEntity: ShopItemEntity): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteShopItem(id: Int): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAll(): Single<MutableList<ShopItemEntity>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getShopItem(id: Int): Single<ShopItemEntity> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun saveCollection(shopItemEntities: MutableList<ShopItemEntity>): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateShopItem(shopItemEntity: ShopItemEntity): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}