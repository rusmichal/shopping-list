package pl.test.shoppinglist.data.source

import javax.inject.Inject

open class ShopItemDataSourceFactory @Inject constructor() {

    fun createRealmDataStore(): ShopItemDataSource {
        return ShopItemRealmData()
    }
}